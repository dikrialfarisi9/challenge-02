import React from 'react';
import {Text, View, Image} from 'react-native';

import {styles} from './styles';

import CardList from '../cardList/index';

export default function CarList() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.txtHeader}>Daftar Mobil</Text>
      </View>
      <View style={styles.content}>
        <CardList />
      </View>
    </View>
  );
}
