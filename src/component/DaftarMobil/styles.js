import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    marginHorizontal: 20,
    marginVertical: 20,
  },
  txtHeader: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  content: {
    marginHorizontal: '3%',
    paddingBottom: '17%',
  },
});
