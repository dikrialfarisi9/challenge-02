import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  productCardList: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 15,
    marginTop: 8,
    marginBottom: 7,
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 4,
    width: '98%',
    height: 105,
    marginHorizontal: '1%',
  },

  shadowProp: {
    shadowColor: '#000',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 3,
  },

  cardImage: {
    flex: 1,
  },
  imgProduct: {
    width: 50,
    height: 30,
    marginBottom: 30,
  },
  cardText: {
    flex: 3,
  },
  loveAndStock: {
    flex: 1,
    flexDirection: 'row',
    marginTop: '2%',
  },
  nameProduct: {
    color: 'black',
    fontSize: 16,
  },
  priceProduct: {
    color: '#5CB85F',
  },
});
