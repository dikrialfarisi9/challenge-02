import React from 'react';
import {Text, View, Image} from 'react-native';
import {FlatList, TouchableOpacity} from 'react-native-gesture-handler';

import Icon from 'react-native-vector-icons/Octicons';

import {styles} from './styles';

import {product} from '../../data/index';

export default function CarList() {
  return (
    <View style={styles.container}>
      <FlatList
        data={product}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <TouchableOpacity style={[styles.productCardList, styles.shadowProp]}>
            <View style={styles.cardImage}>
              <Image style={styles.imgProduct} source={item.image} />
            </View>

            <View style={styles.cardText}>
              <Text style={styles.nameProduct}>{item.title}</Text>
              <View style={styles.loveAndStock}>
                <View style={{flexDirection: 'row'}}>
                  <Icon
                    style={{marginTop: '8%'}}
                    name="people"
                    color={'#8A8A8A'}
                  />
                  <Text style={{marginRight: '5%'}}> {item.love}</Text>
                </View>

                <View style={{flexDirection: 'row'}}>
                  <Icon
                    style={{marginTop: '8%'}}
                    name="briefcase"
                    color={'#8A8A8A'}
                  />
                  <Text> {item.stock}</Text>
                </View>
              </View>
              <Text style={styles.priceProduct}>Rp {item.price}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
}
