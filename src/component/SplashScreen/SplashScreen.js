import React, {useEffect} from 'react';
import {View, Image} from 'react-native';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('MainApp');
    }, 2000);
  }, [navigation]);

  return (
    <View style={{flex: 1, width: '100%', height: '100%'}}>
      <Image
        style={{width: '100%', height: '100%', resizeMode: 'stretch'}}
        source={require('../../asset/splash.png')}
      />
    </View>
  );
};

export default SplashScreen;
