import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    flex: 0.6,
    flexDirection: 'row',
    backgroundColor: '#D3D9FD',
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  txtHeader: {
    flex: 3,
  },
  imgHeader: {
    flex: 1,
  },
  image: {
    width: '40%',
    height: '30%',
    borderRadius: 100,
    alignSelf: 'flex-end',
  },
  banner: {
    width: '90%',
    height: '21%',
    marginHorizontal: '5%',
    borderRadius: 6,
    position: 'absolute',
    marginTop: '20%',
  },
  content: {
    flex: 1,
    paddingHorizontal: 20,
    flexDirection: 'row',
  },
  flatlist: {
    marginTop: '30%',
  },
  imgContent: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DEF1DF',
    width: 55,
    height: 55,
    borderRadius: 8,
  },
  listContent: {
    width: 25,
    height: 25,
  },
  borderContent: {
    marginRight: 21,
    alignItems: 'center',
  },
  shadowProp: {
    shadowColor: '#000',
    shadowOffset: {width: 2, height: 2},
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 3,
  },
  listProduct: {
    flex: 2,
    marginHorizontal: 20,
    marginVertical: 1,
  },
  txtDaftarPilihan: {
    marginTop: '5%',
    marginBottom: '3%',
    color: 'black',
    fontSize: 16,
    fontWeight: 'bold',
  },
  list: {
    marginTop: '2%',
    paddingBottom: '14%',
  },
});
