import React from 'react';
import {Text, View, Image, FlatList, TouchableOpacity} from 'react-native';

import {styles} from './styles';

import CardList from '../cardList/index';

import {menu} from '../../data/index';
import {SafeAreaView} from 'react-native-safe-area-context';

export default function Home() {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <View style={styles.txtHeader}>
          <Text style={{color: 'black'}}>Hi, Dzikri</Text>
          <Text style={{color: 'black', fontWeight: 'bold', fontSize: 15}}>
            Pandeglang - Banten
          </Text>
        </View>
        <TouchableOpacity style={styles.imgHeader}>
          <Image
            style={styles.image}
            source={require('../../asset/dzikri.png')}
          />
        </TouchableOpacity>
      </View>

      <Image
        resizeMode="stretch"
        style={styles.banner}
        source={require('../../asset/Banner.png')}
      />

      <View style={styles.content}>
        <FlatList
          style={styles.flatlist}
          data={menu}
          horizontal
          scrollEnabled={false}
          showsHorizontalScrollIndicator={false}
          renderItem={({item}) => (
            <TouchableOpacity style={styles.borderContent}>
              <View style={[styles.imgContent, styles.shadowProp]}>
                <Image style={styles.listContent} source={item.image} />
              </View>
              <View>
                <Text style={{marginTop: 6, color: 'black', fontSize: 13}}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>

      <View style={styles.listProduct}>
        <View style={styles.daftarPilihan}>
          <Text style={styles.txtDaftarPilihan}>Daftar Mobil pilihan</Text>
        </View>
        <View style={styles.list}>
          <CardList />
        </View>
      </View>
    </SafeAreaView>
  );
}
