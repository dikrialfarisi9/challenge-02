import React from 'react';
import {Text, View, Image} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

import {styles} from './styles';

export default function Akun() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.txtHeader}>Akun</Text>
      </View>
      <View style={styles.content}>
        <Image
          style={styles.imgContent}
          source={require('../../asset/Allura_Park1.png')}
        />
        <Text style={styles.txtContent}>
          Upss kamu belum memiliki akun. Mulai buat akun agar transaksi di BCR
          lebih mudah
        </Text>
        <TouchableOpacity style={styles.btnContent}>
          <Text style={styles.txtBtn}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
