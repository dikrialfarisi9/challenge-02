import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    marginHorizontal: 20,
    marginVertical: 20,
  },
  txtHeader: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 20,
  },
  content: {
    flex: 1,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgContent: {width: '100%', height: 200, margin: 20},
  txtContent: {
    color: 'black',
    fontSize: 15,
    textAlign: 'center',
    marginHorizontal: 12,
    marginBottom: 20,
  },
  btnContent: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#38b000',
    width: 81,
    height: 36,
    borderRadius: 2,
  },
  txtBtn: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
  },
});
