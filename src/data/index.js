const product = [
  {
    id: '1',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '2',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '3',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '4',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '5',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '6',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '7',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '8',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '9',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
  {
    id: '10',
    title: 'Daihatsu Xenia',
    image: require('../asset/xenia.png'),
    price: '230.000',
    stock: '2',
    love: '4',
  },
];

export {product};

const menu = [
  {
    id: '1',
    title: 'Sewa Mobil',
    image: require('../asset/fi_truck.png'),
  },
  {
    id: '2',
    title: 'Oleh-Oleh',
    image: require('../asset/fi_box.png'),
  },
  {
    id: '3',
    title: 'Penginapan',
    image: require('../asset/fi_key.png'),
  },
  {
    id: '4',
    title: 'Wisata',
    image: require('../asset/fi_camera.png'),
  },
];

export {menu};
